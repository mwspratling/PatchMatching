function keypointCandidatesAccepted=exclude_keypoints_close_to_others(keypoints,keypointCandidates,Tbox,numExtra)
numAccepted=0;
keypointCandidatesAccepted=[];
for i=1:size(keypointCandidates,1)
  if numAccepted>=numExtra, break; end %stop when required number of keypoints is reached
  if ~isempty(keypoints) & max(abs(keypointCandidates(i,1)-keypoints(:,1))<Tbox(4) & abs(keypointCandidates(i,2)-keypoints(:,2))<Tbox(3)) %don't include ones that overlap any other template
    %skip
  else
    %add keypoint candidate to list of keypoints so can exclude overlap on future iterations
    keypoints=[keypoints;keypointCandidates(i,:)];
    numAccepted=numAccepted+1;
    keypointCandidatesAccepted(numAccepted,:)=keypointCandidates(i,:);
  end
end

