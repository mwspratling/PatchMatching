function [BBSPath,VGGPath]=set_paths(method)
%required for all methods
BBSPath='../../OtherPeoplesCode/BBS_code_and_data_release_v1.0/';
addpath([BBSPath,'BBS_demo/']); 

if nargout>1
  VGGPath='../../Data/VGG_Affine_Covariant_Features/';
end

switch method
  case 'CoTM'
    %only required if using method CoTM
    CoTMPath='../../OtherPeoplesCode/CoTM_final/';
    addpath(CoTMPath,[CoTMPath,'Utils/']);
  case 'DDIS'
    %only required if using method DDIS
    DDISPath='../../OtherPeoplesCode/DDIS-master/DDIS_code_matlab/';
    addpath(genpath(DDISPath));
end
%dataPath=[BBSPath,'data/'];
%dataPath=[CoTMPath,'Data/'];
