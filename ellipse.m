function s=ellipse(alen,blen)
%define grid of x,y coodinates at which to define function
[x y]=meshgrid(-alen:alen,-blen:blen);
 
s=zeros(size(x));
%define ellipse
s(((x.^2/alen^2) + (y.^2/blen^2))<=1)=1;
