function [AUC]=expt_num_extra_templates_bbs_data

extraTemplates=[0:6,8,10:5:30];
k=0;
for numExtraTemplates=extraTemplates
  k=k+1;
  [~,~,AUC(k)]=stats_correspondence_bbs_data('DIM',[],numExtraTemplates);
  disp(['... obtained using ',int2str(numExtraTemplates),' extra templates'])
end
figured(203); clf, subplot(1,3,2:3)
plot(extraTemplates,AUC(1,:),'r-x','linewidth',3,'MarkerSize',10);
%hold on
%plot(extraTemplates,AUC(2,:),'r:','linewidth',3,'MarkerSize',10);
%plot(extraTemplates,AUC(3,:),'r-.','linewidth',3,'MarkerSize',10);
axis([0,max(extraTemplates),0.55,0.7])
xlabel('additional templates');
ylabel('AUC'); 
legend({'correl','sift','random'},'Location','SouthEast')
set(gca,'FontSize',20);
set(gca,'OuterPosition',[0.2611   0    0.6992    1]) %stop xlabels falling off ege of page
%print_fig('bbs_data_AUC_vs_additional_templates_DIM.pdf');
