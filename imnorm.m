function [Ihf,Idc,Ion,Ioff,range]=imnorm(I,sigma,gain,leavepadded,pad)
if nargin<2 || isempty(sigma), sigma=2; end
if nargin<3 || isempty(gain), gain=2; end
if nargin<4 || isempty(leavepadded), leavepadded=0; end
if nargin<5 || isempty(pad)
  pad=ceil(4.*sigma);
  pad=[pad,pad];
end

I=padarray(I,pad,'symmetric');
[a,b]=size(I);

%g=gauss2D(sigma); %2D Gaussian
%Idc=conv2(I,g,'same');
g=gauss2D(sigma,[],[],[],[1,odd(6*sigma)]); %1D Gaussian
Idc=conv2(conv2(I,g,'same'),g','same'); %use Gaussian separability to speed up convolution with 2D Gaussian
Ihf=I-Idc; %so I can be reconstructed as I=Idc+Ihf
Ihf=gain.*Ihf;
%Ihf=tanh(Ihf);

range{1}=pad(1)+1:a-pad(1);
range{2}=pad(2)+1:b-pad(2);
if ~leavepadded
  Ihf=Ihf(range{1},range{2},:);
  Idc=Idc(range{1},range{2},:);
  [a,b]=size(Idc);
  range{1}=1:a;
  range{2}=1:b;
end

Ion=Ihf;   Ion(Ion<0)=0;   
Ioff=-Ihf; Ioff(Ioff<0)=0;

if iszero(Ihf), disp('WARNING: - imnorm gaussian too small, output entirely low frequency'); end
