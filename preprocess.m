function [I,X,trueRange]=preprocess(I,sigmaLGN,trueRangeReqd,pad)
%pre-processes images / templates.
%
%If input has 3 channels, it is assumed that it is a RGB image and is converted to Lab colourspace.
% 
%Each channel of the image is converted to two on/off channels

numChannels=size(I,3);
if numChannels==3
  %convert images to LAB colour space
  I=single(applycform(double(I),makecform('srgb2lab'))); I=I./100; 
end

if nargout>1
  for colour=1:numChannels
    [~,~,X(:,:,2*(colour-1)+1),X(:,:,2*(colour-1)+2),trueRange]=imnorm(I(:,:,colour),sigmaLGN,[],trueRangeReqd,pad);
  end
end
