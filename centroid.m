function [centx,centy]=centroid(W,x,y)
%calculate the "centre of mass" of a matrix 2D matrix
[a,b]=size(W);
if nargin<2 || isempty(x)
  x=1:b;
end
if nargin<3 || isempty(y)
  y=1:a;
end

sumW = max(1e-9,sum(sum(W)));

centx = sum(sum(W,1).*x)/sumW;
centy = sum(sum(W,2)'.*y)/sumW;
