function lineType=plot_styles(method)
switch method
  case {'DIM', 'DIMinv'}
    lineType='r-x';
  case {'ZNCC', 'ZNCCinv'}
    lineType='b-s';
  case 'BBS'
    lineType='c-o';
  case 'DDIS'
    lineType='g-d';
  case 'CoTM'
    lineType='y-v';    
  otherwise
    disp(['WARNING: unknown method; ',method])
    lineType='k-d';
end
 