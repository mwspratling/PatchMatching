function [y,Pbox]=template_matching_cotm(I,Tboxes,Iquery,templates)
%This function performs template matching usingCo-Occurrence Template Matching
%(CoTM). The code is adapted from the CoTM code available from:
%http://www.eng.tau.ac.il/~avidan/
%
%The input is: an image, I; the location, defined by Tboxes, of a patch of this
%image; and a second image, Iquery. This function uses the target location in
%the first image as a template for finding the corresponding location in the
%second image. The output is an array, y, the same size as the second image
%that denotes the strength of the match between the template and each location
%in the second image.
%
%If Tboxes contains multiple rows, then template matching is performed for each
%template defined by each row, and y will by a three-dimensional array
%containing the similarity for each template to the image.
%
%If the input variable templates is specified, I and Tboxes are ignored and
%the given templates are matched to Iquery

[a2,b2,c]=size(Iquery);

if nargin<4
  numTemplates=size(Tboxes,1);
else
  numTemplates=length(templates);
end

y=zeros(a2,b2,numTemplates,'single'); %pre-allocate memory
for t=1:numTemplates
  if nargin<4
    %extract template from given image
    Tbox=round(Tboxes(t,:));
    T=imcrop(I,Tbox-[0,0,1,1]); %imcrop produces patches that are one pixel larger
                                %than requested, this produces the correct width
                                %and height template: copyied method used in
                                %CoTM code
  else
    %use supplied template
    Tbox=[1,1,size(templates{t},2),size(templates{t},1)];
    T=templates{t};
  end
    
  %perform matching of the template to the second image using BBS matching and
  %find location where template has best match (using CoTM code)
  [y(:,:,t),Pbox(t,:)]=CoTM(T,Iquery,256);
end
