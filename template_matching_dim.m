function [y]=template_matching_dim(I,Tboxes,Iquery,numAdditionalTemplates,Ibackground,templates,iterations)
%This function performs template matching using PC/BC-DIM.
%
%The input is: an image, I; the location, defined by Tbox, of a patch of
%this image; and a second image, Iquery. This function uses the target location
%in the first image as a template for finding the corresponding location in the
%second image. The output is an array, y, the same size as the second image
%that denotes the strength of the match between the template and each location
%in the second image.
%
%If Tboxes contains multiple rows, then template matching is performed for each
%template defined by each row, and y will by a three-dimensional array
%containing the similarity for each template to the second image.
%
%numAdditionalTemplates specifies the number of non-target templates to use. These
%are extracted from Ibackground unless this is not specified, in which case they
%are extracted from I.
%
%If the input variable templates is specified, I, Tboxes numAdditionalTemplates and
%Ibackground are ignored and the given templates are matched to Iquery

if nargin<7 || isempty(iterations), iterations=10; end

if nargin>5
  %apply DIM to find similarity between each given template and Iquery

  [height,width,numChannels]=size(templates{1});
  sigmaLGN=min([width,height])/2;
  pad=ceil([height,width]);
  numTargets=length(templates);
else
  %calc similarity between templates extracted from image I and Iquery

  correl=1; %define method for selection additional templates
  sigmaLGN=min(mean(Tboxes(:,3:4),1))/2;
  pad=ceil(fliplr(mean(Tboxes(:,3:4),1)));
  numTargets=size(Tboxes,1);

  [I,XI]=preprocess(I,sigmaLGN,0,pad);
  if nargin<4 || isempty(numAdditionalTemplates), numAdditionalTemplates=4; end
  if nargin<5 || isempty(Ibackground), 
    Ibackground=I; 
    XIbackground=XI;
    uniqueBackground=0;
  else
    [Ibackground,XIbackground]=preprocess(Ibackground,sigmaLGN,0,pad);
    uniqueBackground=1;
    disp('using Ibackground');
  end
  
  additionalKeypoints=[];
  for t=1:numTargets
    [templates{t},oddTbox]=imcrop_odd(XI,Tboxes(t,:));
    targetKeypoints(t,:)=[oddTbox(2)+(oddTbox(4)-1)/2,oddTbox(1)+(oddTbox(3)-1)/2];
    
    if correl & numAdditionalTemplates>0 
      %extract additional templates from distinct regions of the original image - do this for each template in turn if using "correl"
      additionalKeypointsTarget=extract_keypoints(Ibackground,Inf,'correl',templates{t},XIbackground);
      additionalKeypointsTarget=exclude_keypoints_close_to_border(additionalKeypointsTarget,Ibackground,oddTbox);
      if uniqueBackground
        additionalKeypointsTarget=exclude_keypoints_close_to_others(additionalKeypoints,additionalKeypointsTarget,oddTbox,numAdditionalTemplates);
      else
        additionalKeypointsTarget=exclude_keypoints_close_to_others([targetKeypoints;additionalKeypoints],additionalKeypointsTarget,oddTbox,numAdditionalTemplates);
      end
      additionalKeypoints=[additionalKeypoints;additionalKeypointsTarget];
    end
  end
  if ~correl & numAdditionalTemplates>0 
    %extract additional templates from distinct regions of the original image - do this in one go if using method other than 'correl'
    additionalKeypoints=extract_keypoints(Ibackground(:,:,1),numAdditionalTemplates*numTargets*100,'sift');%'random');%
    additionalKeypoints=exclude_keypoints_close_to_border(additionalKeypoints,Ibackground,oddTbox);
    if uniqueBackground
      additionalKeypoints=exclude_keypoints_close_to_others([],additionalKeypoints,oddTbox,numAdditionalTemplates*numTargets);
    else
      additionalKeypoints=exclude_keypoints_close_to_others(targetKeypoints,additionalKeypoints,oddTbox,numAdditionalTemplates*numTargets);
    end
  end
  numAdditionalTemplates=size(additionalKeypoints,1)
  
  %create additional templates: note assumes all additional templates are the same size as the last defined template
  for t=1:numAdditionalTemplates
    templates{numTargets+t}=imcrop_odd(XIbackground,[additionalKeypoints(t,2)-(oddTbox(3)-1)/2,additionalKeypoints(t,1)-(oddTbox(4)-1)/2,oddTbox(3),oddTbox(4)]);
  end
end

%show templates (for targets and additional, non-target, templates)
%for t=1:numTargets+numAdditionalTemplates, maxsubplot(4,10,t);plot_image(templates{t}(:,:,1)-templates{t}(:,:,2)); end, drawnow


%perform template matching using DIM
[~,X,trueRange]=preprocess(Iquery,sigmaLGN,1,pad);
y=dim_activation_conv(templates,X,[],[],iterations,trueRange);
y=y(:,:,1:numTargets); %remove similarity arrays for additional, non-target, templates

%sum responses over a small region:
scale=0.025;
region=ellipse(round(max(1,scale*pad(2))),round(max(1,scale*pad(1))));
%region=gauss2D(scale*pad(1),0,pad(2)/pad(1),0);
for j=1:numTargets
  y(:,:,j)=conv2(y(:,:,j),region,'same');
end
