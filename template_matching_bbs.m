function [y,Pbox]=template_matching_bbs(I,Tboxes,Iquery,templates)
%This function performs template matching using Best Buddies Similarity
%matching. The code is adapted from, and makes calls to, the BBS demo provided
%by Shaul Oron (shauloron (at) gmail (dot) com).
%
%The input is: an image, I; the location, defined by Tboxes, of a patch of
%this image; and a second image, Iquery. This function uses the target location
%in the first image as a template for finding the corresponding location in the
%second image. The output is: an array, y, the same size as the second image
%that denotes the strength of the match between the template and each location
%in the second image.
%
%If Tboxes contains multiple rows, then template matching is performed for each
%template defined by each row, and y will by a three-dimensional array
%containing the similarity for each template to the image.
%
%If the input variable templates is specified, I and Tboxes are ignored and
%the given templates are matched to Iquery

%convert images to HSV colour space
%[a,b,c]=size(I);
%if c>1
%  I=rgb2hsv(I);
%  Iquery=rgb2hsv(Iquery);
%end

szI=size(Iquery);

% set BBS params
gamma = 2; % weighing coefficient between Dxy and Drgb
pz = 3; % non-overlapping patch size used for pixel descirption

if nargin<4
  numTemplates=size(Tboxes,1);
  T=imcrop(I,Tboxes(1,:));
else
  numTemplates=length(templates);
  Tboxes=[1,1,size(templates{1},2),size(templates{1},1)];
  T=templates{1};
end

[Iquery,T,Tbox,I]=adjustImageSize(Iquery,T,Tboxes(1,:),I,pz);
y=zeros(size(Iquery,1),size(Iquery,2),numTemplates,'single'); %pre-allocate memory
for t=1:numTemplates
  if nargin<4
    %extract template from given image
    Tbox=round(Tboxes(t,:));
    T=imcrop(I,Tbox);
  else
    %use supplied template
    Tbox=[1,1,size(templates{t},2),size(templates{t},1)];
    T=templates{t};
  end
  %adjust image and template size so they are divisible by the patch size 'pz'
  [Iquery,T,Tbox,I]=adjustImageSize(Iquery,T,Tbox,I,pz);
  szT=size(T);

  %perform matching of the template to the second image using BBS matching
  ytmp=computeBBS(Iquery,T,gamma,pz);
  %interpolate match strength array back to image size
  y(:,:,t)=BBinterp(ytmp,szT(1:2),pz,NaN);

  %find location where template has best match (using BBS code)
  Pbox(t,:)=findTargetLocation(y(:,:,t),'max',Tbox(3:4));
end
y(isnan(y))=0; %remove NaNs that BBS inserts around border of image



