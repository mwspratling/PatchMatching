function f1=calc_f1score(stats)
TP=stats(:,1);
FP=stats(:,2);
FN=stats(:,3);

f1=2.*TP./(2.*TP+FP+FN);
