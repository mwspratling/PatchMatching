function keypoints=extract_keypoints(I,num,method,template,X,priority)
if nargin<3 || isempty(method)
  method='corner';
end
disp(['extract_keypoints ',method]);

[a,b,c]=size(I);
if c==1,
  Igray=I;
elseif c==3,
  Igray=rgb2gray(I);
else
  disp('WARNING: unknow image type');
end

switch method
  case 'random'
    keypoints = random_keypoints(Igray,num); if c==3, disp('assumed RGB image'); end
  case 'corner'
    keypoints = corner(Igray',num); if c==3, disp('assumed RGB image'); end
  case 'sift'
    if ~exist('vl_version.m','file')
      run('/home/mike/Utils/Matlab/Toolboxes/vlfeat-0.9.20/toolbox/vl_setup');
    end
    keypoints = round(vl_sift(single(Igray),'PeakThresh',0.02,'EdgeThresh',2,'NormThresh',0)); 
    if c==3, disp('assumed RGB image'); end
    keypoints = fliplr(keypoints(1:2,:)');
  case 'correl'
    coef=patch_similarity(template,X);
    %coef=template_matching_dim([],[],I,0,[],{template},1);
    [val,ind]=sort(coef(:),'descend');
    [keypoints(:,1),keypoints(:,2)]=ind2sub(size(coef),ind);
   case 'priority'
    [val,ind]=sort(priority(:),'descend');
    [a,b]=size(priority);[x,y]=meshgrid(1:b,1:a);
    keypoints(:,1)=y(ind);
    keypoints(:,2)=x(ind);
end
keypoints=keypoints(1:min(size(keypoints,1),num),:);


function similarity=patch_similarity(template,X)    
similarity=0;
nChannels=size(template,3);
template=rot90(template,2);
for j=1:nChannels
  similarity=similarity+conv2(X(:,:,j),template(:,:,j),'same');
end


function points=random_keypoints(I,num)

[a,b]=size(I);
points(1:num*2,1)=randi(a,num*2,1);
points(1:num*2,2)=randi(b,num*2,1);
points=unique(points,'rows');
ind=randperm(size(points,1));
points=points(ind(1:min(num,length(ind))),1:2);

