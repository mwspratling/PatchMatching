function [BBoverlap,BBoverlapTop7,AUC,time]=stats_correspondence_bbs_data(method,imagePairs,numExtraTemplates,background)
%Tests template matching methods using the same dataset and methedology as used to
%test the Best-Buddies Similarity method (see T. Dekel, S. Oron, M. Rubinstein,
%S. Avidan and W.T. Freeman. Best Buddies Similarity for Robust Template
%Matching, IEEE Conference on Computer Vision and Pattern Recognition (CVPR),
%2015).

if nargin<1 || isempty(method)
  method='DIM'; %'DDIS'; %'ZNCC'; %'BBS'; %'CoTM'; %
end
if nargin<2 || isempty(imagePairs)
  imagePairs=[1:105];
end
if nargin<3 || isempty(numExtraTemplates), numExtraTemplates=4; end

[BBSPath,VGGPath]=set_paths(method);

if nargin<4 || isempty(background) || background==0
  %extract additional, non-target, templates from the 1st image in the pair
  Ibackground=[]; 
else
  %define an image from which to extract additional, non-target, templates
  Ibackground=im2double(imread([VGGPath,'Leuven/img1.ppm'])); 
end

tic
for i=imagePairs
  disp(['####### IMAGE ',int2str(i),' #######']);
  
  %locate files from which to load data
  iStr=sprintf('%04d',i);
  imPairFiles = dir([BBSPath,'data/pair',iStr,'*.jpg']);
  boundingBoxPairFiles = dir([BBSPath,'data/pair',iStr,'*.txt']);

  %load first image in the pair and location of region that is to be matched to second image
  I=im2double(imread([BBSPath,'data/',imPairFiles(1).name]));
  Tbox=load([BBSPath,'data/',boundingBoxPairFiles(1).name]);

  %show first image and target bounding box
  figured(1),clf, maxsubplot(1,3,1); plot_image(rgb2gray(I)); 
  plot_bounding_box(Tbox);

  %load second image in the pair and the location of the true match 
  Iquery=im2double(imread([BBSPath,'data/',imPairFiles(2).name]));
  GTbox=load([BBSPath,'data/',boundingBoxPairFiles(2).name]);

  %perform template matching
  switch method
    case 'DIM'
      [y]=template_matching_dim(I,Tbox,Iquery,numExtraTemplates,Ibackground);Pbox=[];
    case 'ZNCC'
      [y]=template_matching_zncc(I,Tbox,Iquery);Pbox=[];
    case 'BBS'
      [y,Pbox]=template_matching_bbs(I,Tbox,Iquery);
    case 'CoTM'
      [y,Pbox]=template_matching_cotm(I,Tbox,Iquery);
    case 'DDIS'
      [y,Pbox]=template_matching_ddis(I,Tbox,Iquery);
  end
  %evaluate performance
  [Pbox,BBoverlap(i),~,BBoverlapTop7(i)]=analyse_match_array_maxsimilarity(y,Tbox,GTbox,Pbox);
  
  %show second image and ground-truth bounding box
  maxsubplot(1,3,2); plot_image(rgb2gray(Iquery)); 
  plot_bounding_box(GTbox);
  %show predicted location of the target in the second image
  plot_bounding_box(Pbox,'c');
  %show similarity array
  maxsubplot(1,3,3);
  plot_image(-y); %plot -ve values to produce image with inverted colormap
  colormap('gray');drawnow;
end
time=toc;
disp(' ')
%plot results for successful localisation of image patch as a function of acceptable bounding-box overlap
figured(201); clf; AUC=plot_success_curve(BBoverlap,method);
%compare results with those for BBS
plot_success_curve_bbs_data([BBSPath,'results/Final_results_CVPR15_all_methods.mat'])
legend({method;'BBS'},'Location','SouthWest')


%plot results for successful localisation of image patch as a function of acceptable bounding-box overlap for best match in top 7
figured(202); clf; plot_success_curve(BBoverlapTop7,method);
%compare results with those for BBS
plot_success_curve_bbs_data([BBSPath,'results/Final_results_CVPR15_all_methods_top7.mat'])

%methods={'ZNCC';'BBS';'DDIS';'DIM'};
%for i=1:4, [BBoverlap{i},BBoverlapTop7{i},AUC(i),time(i)]=stats_correspondence_bbs_data(methods{i}); end
%for i=1:4, plot_success_curve(BBoverlap{i},methods{i}); end, legend(methods,'Location','SouthWest'); print_fig('bbs_data_success_ALL.pdf')
%for i=1:4, plot_success_curve(BBoverlapTop7{i},methods{i}); end, legend(methods,'Location','SouthWest'); print_fig('bbs_data_success_top7_ALL.pdf')