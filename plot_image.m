function plot_image(I,scale)
if nargin<2 || isempty(scale)
  imagesc(I);
else
  imagesc(I,scale);
end
axis('equal','tight'); set(gca,'XTick',[],'YTick',[]);