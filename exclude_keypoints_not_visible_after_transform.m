function keypoints=exclude_keypoints_not_visible_after_transform(keypoints,H,Iquery,patchHalfLen)

%find location of keypoints on query image
keypoints_trans=project_keypoint(keypoints,H);

%remove keypoints that are not visible in 2nd image
[a,b,c]=size(Iquery);
unseeable=find(keypoints_trans(:,1)<1+patchHalfLen | keypoints_trans(:,1)>a-patchHalfLen | keypoints_trans(:,2)<1+patchHalfLen | keypoints_trans(:,2)>b-patchHalfLen);
keypoints(unseeable,:)=[];

