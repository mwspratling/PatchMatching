function gauss=gauss2D(sigma,orient,aspect,norm,pxsize,cntr,order)
%function gauss=gauss2D(sigma,orient,aspect,norm,pxsize,cntr,order)
%
% This function produces a numerical approximation to Gaussian function with
% variable aspect ratio.
% Parameters:
% sigma  = standard deviation of Gaussian envelope, this in-turn controls the
%          size of the result (pixels)
% orient = orientation of the Gaussian clockwise from the vertical (degrees)
%          Optional, if not specified orient=0.
% aspect = aspect ratio of Gaussian envelope (0 = no "length" to envelope, 
%          1 = circular symmetric envelope)
%          Optional, if not specified aspect=1.
% norm   = 1 to normalise the gaussian so that it sums to 1
%        = 0 for no normalisation (gaussian has max value of 1)
%          Optional, default value is 1.
% pxsize = the size of the filter.
%          Optional, if not specified size is 6*sigma*max(1,aspect)
% cntr   = location of the centre of the gaussian.
%          Optional, if not specified gaussian is centred in the middle of image.
% order  = order of differential. Differential is calculated in the y direction.
%          Valid values are whole numbers from 0 (output is not differentiated) 
%          to 4 (output is the 4th differential of a gaussian).
%          Optional, if not specified default is 0.

if nargin<2 || isempty(orient), orient=0; end
if nargin<3 || isempty(aspect), aspect=1; end
if nargin<4 || isempty(norm), norm=1; end
if nargin<5 || isempty(pxsize), pxsize=[1,1].*odd(6*sigma*max(1,aspect)); end
if nargin<6 || isempty(cntr), cntr=0.5+pxsize./2; end
if nargin<7 || isempty(order), order=0; end 

scale=1; %0.2;0.04

%define grid of x,y coodinates at which to define function
[x y]=meshgrid(1-0.5+scale/2:scale:pxsize(1)+0.5-scale/2,...
               1-0.5+scale/2:scale:pxsize(2)+0.5-scale/2);
 
%rotate 
orient=-orient*pi/180;
x_theta=(x-cntr(1))*cos(orient)+(y-cntr(2))*sin(orient);
y_theta=-(x-cntr(1))*sin(orient)+(y-cntr(2))*cos(orient);

%avoid division by zero errors
sigma=max(1e-15,sigma);

%define gaussian
gauss=exp(-.5*( ((x_theta.^2)./((sigma*aspect).^2)) ...
                + ((y_theta.^2)./(sigma.^2)) ));
gauss=gauss./(sigma*sqrt(2*pi));

%differentiate gaussian, if requested
if order==0
  %do nothing
elseif order==1
  gauss=gauss.*(-y_theta./(sigma^2));
elseif order==2
  gauss=gauss.*(((y_theta.^2)-(sigma^2))./(sigma^4));
elseif order==3
  gauss=gauss.*(-y_theta.*((y_theta.^2)-(3*sigma^2))./(sigma^6));
elseif order==4
  gauss=gauss.*(((y_theta.^4)-(6.*y_theta.^2.*sigma^2)+(3*sigma^4))./(sigma^8));
else
  disp('ERROR: order of differential applied to gaussian, not defined');
end
gauss=imresize(gauss,scale);

%normalise
if norm, 
  gauss=gauss./sum(sum(abs(gauss))); 
else
  gauss=gauss./max(max(abs(gauss)));
end
