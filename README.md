----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the simulation results reported in:

[M. W. Spratling (2020) Explaining Away Results in Accurate and Tolerant Template Matching, 
Pattern Recognition, 104 (107337).](https://nms.kcl.ac.uk/michael.spratling/Doc/dim_patchmatching.pdf)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# REQUIREMENTS
----------------------------------------------------------------------------

This code was tested with MATLAB Version 9.3.0.713579 (R2017b) and MATLAB
Version 9.4.0.813654 (R2018a) running on Ubuntu Linux version 16.04.


## Dependencies:

The code uses some functions from the MATLAB Image Processing Toolbox.

The code uses the dataset and some functions from the Best-Buddies Template Matching code
(version 1.0) available from:
http://people.csail.mit.edu/talidekel/Best-Buddies%20Similarity.html
The variable BBSPath in set_paths.m should be edited to point to the location
where this code is installed.


## Optional Dependencies:

Some functions use the Oxford VGG Affine Covariant Features Dataset which is
available from: http://www.robots.ox.ac.uk/~vgg/research/affine/ To use these
functions this data should be downloaded and the variable VGGPath in set_paths.m
should be edited to point to the location of this dataset.

The code can also be used to test the DDIS and CoTM algorithms. To do so the
code from https://github.com/roimehrez/DDIS and
http://www.eng.tau.ac.il/~avidan/ should be installed and the variables DDISPath
and CoTMPath in the file set_paths.m should be edited to point to the locations
where this code is installed.

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

To re-run the experiments described in the paper, do the following.

Fig. 1 can be reproduced using the command:
```
    stats_correspondence_bbs_data(method,imagePair);
```
    where method is set to the name of the algorithm to be tested (either 'DIM',
    'ZNCC', 'BBS' or 'DDIS'), and imagePair is an integer between 1 and 105
    specifying which image pair from the BBS dataset to use.
    e.g. to reproduce the bottom-right panel, run: stats_correspondence_bbs_data('DIM',88);

Fig. 2 (and the corresponding values in Table 1) can be reproduced using the command:
```
    stats_correspondence_bbs_data(method);
```
    where method is set to the name of the algorithm to be tested (either 'DIM',
    'ZNCC', 'BBS' or 'DDIS')
    e.g. to reproduce the red curve, run: stats_correspondence_bbs_data('DIM');

Fig. 3 can be reproduced using the command:
```
    expt_num_extra_templates_bbs_data;
```     
    To produce results with different methods of selecting the additional
    templates requires editing of function template_matching_dim

Fig. 4 (and the corresponding values in Table 2) can be reproduced using the command:
```
    stats_correspondence_vgg_data(method);
```
    where method is set to the name of the algorithm to be tested (either 'DIM',
    'ZNCC', 'BBS' or 'DDIS')
    e.g. to reproduce the top-left panel, run: stats_correspondence_vgg_data('ZNCC');
    To produce results with different sized templates, provide a value of 1, 2,
    or 3 as the fourth input variable of the function (these numbers correspond
    to the three columns of Fig. 4).
    e.g. to reproduce the bottom-right panel, run: stats_correspondence_vgg_data('DIM',[],[],3);
    Note that variable patchHalfLen defines the size of the patches before
    downsampling.

Fig. 5 (and the corresponding values in Table 3) can be reproduced using the command:
```
    stats_template_matching_vgg_data(method);
```
    where method is set to the name of the algorithm to be tested (either 'DIM',
    'ZNCC', 'BBS' or 'DDIS').
    e.g. to reproduce the blue curve in the left panel run:
    stats_template_matching_vgg_data('ZNCC');
    To produce results with different sized templates, provide a value of 1, 2,
    or 3 as the fourth input variable of the function (these numbers correspond
    to the three columns of Fig. 5).
    e.g. to reproduce the red curve in the right panel, run:
    stats_template_matching_vgg_data('DIM',[],[],3);
    Note that variable patchHalfLen defines the size of the patches before
    downsampling.
    
