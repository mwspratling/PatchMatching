function keypoints=exclude_keypoints_close_to_border(keypoints,I,Tbox)
[a,b,c]=size(I);

%remove keypoints too close to edge
keypoints(keypoints(:,1)<=ceil(Tbox(4)/2) | keypoints(:,1)>=a+1-ceil(Tbox(4)/2) | keypoints(:,2)<=ceil(Tbox(3)/2) | keypoints(:,2)>=b+1-ceil(Tbox(3)/2),:)=[];
