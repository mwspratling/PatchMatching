function plot_precision_recall(stats,method)
TP=stats(:,1);
FP=stats(:,2);
FN=stats(:,3);

precision=TP./(TP+FP);
recall=TP./(TP+FN);
lineType=plot_styles(method);
plot(1-precision,recall,lineType,'LineWidth',3,'MarkerSize',10,'MarkerFaceColor',lineType(1))

axis('equal','tight');axis([0,1,0,1])
xlabel('1-precision')
ylabel('recall')
set(gca,'FontSize',20);
drawnow