function [Ipatch,box]=imcrop_odd(I,box)
%crops the image, I, using the coordinates given in box. box has the format:
%[x-coord of top left corner, y-coord of top-left corner, width, height].
%
%The coordinates of the top-left corner are rounded to the nearest integer value.
%The width/height of the patch is rounded up an odd integer value. 
%This behaviour differs from the MATLAB function "imcrop" which generates
%patches of odd and even dimensions, but always 1 pixel larger in width and
%height than was requested.

[a,b,c]=size(I);
box=[round(box(1)),round(box(2)),odd(box(3)),odd(box(4))];
boxInbounds=[min(1+b-odd(box(3)),max(1,round(box(1)))),min(1+a-odd(box(4)),max(1,round(box(2)))),odd(box(3)),odd(box(4))];
if ~isequal(box,boxInbounds)
  disp(['WARNING: crop moved to be within image. ',int2str(box),' --> ',int2str(boxInbounds)]);
  box=boxInbounds;
end
Ipatch=I(box(2)+[0:box(4)-1],box(1)+[0:box(3)-1],:);