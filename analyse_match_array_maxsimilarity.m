function [Pbox,BBoverlap,stats,BBoverlapTop7]=analyse_match_array_maxsimilarity(y,Tbox,GTbox,Pbox,reqdDistance)
%This function analyses the accuracy of template matching.
%
%The input is an array, y, showing the strength with which the template matches
%an image at every location; Tbox which defines the size of the template; and
%GTbox, that defines the true location that the template should match in the
%image. The output is: the location, Pbox, of the location of the best match;
%the overlap, BBoverlap, between the predicted bounding box and the actual
%location of the target; and BBoverlapTop7, the maximum overlap between the
%seven best matches found by template matching and the true location of the
%patch.
%If Pbox is provided as the input, then this code uses that location as the
%predicted location of the template match: this is to allow methods such as CoTM
%and BBS to use their own method of finding the best matching location (these
%produce slightly different results to the method used here).

%calculate bounding box overlap to be able to calculate success curve
if nargin<4 || isempty(Pbox)
  %find location where template has best match 
  y=imregionalmaxSingle(y).*y; %suppress matches that are not local-maxima  
  [peak,ind]=max(y(:));
  [ptx,pty]=ind2sub(size(y),ind);
  Pbox=[pty-(odd(Tbox(3))-1)/2,ptx-(odd(Tbox(4))-1)/2,Tbox(3),Tbox(4)];
end
%compare predicted location to expected match location
BBoverlap=rectOverlap(rectCorners(GTbox),rectCorners(Pbox)); %call BBS code to calc bounding box overlap
%BBoverlap=calcRectNormalizedOverlap(GTbox,Pbox); %call CoTM code to calc bounding box overlap
disp(['GTbox=',num2str(GTbox,'%.1f,'),'  Pbox=',num2str(Pbox,'%.1f,'), '  Overlap=',num2str(BBoverlap)])

if nargin>4
  %calculate number of true-matches, etc., using distance as the criteria
  if isnan(GTbox)
    %no true corresponding point
    TP=0; FP=1; FN=0;
  else
    if sqrt(sum((GTbox(1:2)-Pbox(1:2)).^2))<=reqdDistance
      %Euclidean distance to true corresponding location is within threshold
      TP=1; FP=0; FN=0; 
    else
      %Euclidean distance to true corresponding location is too large
      TP=0; FP=1; FN=1; 
    end
  end
  stats=[TP,FP,FN];
else
  stats=[];
end

%calculate bounding box overlap for multiple best matches to be able to calculate Top7 success curve
if nargout>3
  y=imregionalmaxSingle(y).*y; %suppress matches that are not local-maxima
  %find multiple locations where the template has best matches and compare these to expected match location
  [peaks,inds]=sort(y(:),'descend');
  BBoverlapTop7=BBoverlap;
  for k=2:7
    [ptx,pty]=ind2sub(size(y),inds(k));
    Pboxk=[pty-Pbox(3)/2,ptx-Pbox(4)/2,Pbox(3),Pbox(4)];
    BBoverlapk=rectOverlap(rectCorners(GTbox),rectCorners(Pboxk)); %call BBS code to calc bounding box overlap
    %BBoverlapk=calcRectNormalizedOverlap(GTbox,Pboxk); %call CoTM code to calc bounding box overlap
    BBoverlapTop7=max(BBoverlapk,BBoverlapTop7);
  end
end
