function plot_success_curve_bbs_data(fileName)
load(fileName);
methodsWithResults=fields(ROC);
lineType=plot_styles('BBS');
plot(thROC,ROC.(methodsWithResults{1}),lineType,'linewidth',3,'MarkerSize',10,'MarkerFaceColor',lineType(1));
drawnow