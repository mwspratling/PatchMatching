function [y,Pbox]=template_matching_ddis(I,Tboxes,Iquery,templates)
%This function performs template matching using Deformable Diversity Similarity
%matching. The code is adapted from, and makes calls to, the DDIS code:
%https://github.com/roimehrez/DDIS
%
%The input is: an image, I; the location, defined by Tbox, of a patch of
%this image; and a second image, Iquery. This function uses the target location
%in the first image as a template for finding the corresponding location in the
%second image. The output is an array, y, the same size as the second image
%that denotes the strength of the match between the template and each location
%in the second image.
%
%If Tboxes contains multiple rows, then template matching is performed for each
%template defined by each row, and y will by a three-dimensional array
%containing the similarity for each template to the image.
%
%If the input variable templates is specified, I and Tboxes are ignored and
%the given templates are matched to Iquery

%set DDIS params
pz = 3; % non-overlapping patch size used for pixel description

[a2,b2,c]=size(Iquery);

%method used by DDIS to deal with grayscale images (copied from DDIS code)
if c==1 
  I = cat(3,I,I,I); 
  Iquery = cat(3,Iquery,Iquery,Iquery);
end

if nargin<4
  numTemplates=size(Tboxes,1);
else
  numTemplates=length(templates);
end

y=zeros(a2,b2,numTemplates,'single'); %pre-allocate memory
for t=1:numTemplates
  if nargin<4
    %extract template from given image
    Tbox=round(Tboxes(t,:));
    T=imcrop(I,Tbox);
  else
    %use supplied template
    Tbox=[1,1,size(templates{t},2),size(templates{t},1)];
    T=templates{t};
  end

  %perform matching of the template to the second image using DDIS matching and
  %find location where template has best match (using DDIS code)
  [y(:,:,t),Pbox(t,:)]=computeDDIS(Iquery,T,pz,1,0); 
end
