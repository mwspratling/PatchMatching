function BW=imregionalmaxSingle(y)
%returns locations of local maxima that are single pixels.
%
%This function is the same as the built-in function imregionalmax, except any
%local maximum that contains multiple points is replaced by a single point
%closest to the centroid of that region of maximum values.
BW=imregionalmax(y);
CC=bwconncomp(BW);
for i=1:CC.NumObjects,
  inds=CC.PixelIdxList{i};
  %if regional peak contains multiple points, replace by central one
  if (numel(inds) > 1),
    BW(inds) = false;
    [ptx,pty]=ind2sub(size(y),inds);
    ptx=round(mean(ptx)); 
    pty=round(mean(pty));
    BW(ptx,pty)=true;
  end
end