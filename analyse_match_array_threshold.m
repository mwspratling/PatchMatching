function [stats]=analyse_match_array_threshold(y,patchHalfLen,GTbox,reqdOverlap,thres)
%This function analyses the accuracy of template matching.
%
%The input is an array, y, showing the strength with which the template matches
%an image at every location; patchHalfLen which defines the size of the template; GTbox,
%that defines the true location that the template should match in the image; and
%thres that defines what values of y are considered to indicate template
%matches. The code determines, for all locations of y that are above the
%threshold and local maxima, if any of those matches are sufficiently close to
%the extected location to count as a true match, it also calculates how many
%matches are false positives and how many false negatives. The output is a
%vector containing the number true positives, false positives and false
%negatives.

%find all locations where template match is stronger than the threshold
y=imregionalmaxSingle(y).*y; %suppress matches that are not local-maxima
[peaks,inds]=sort(y(:),'descend');
exclude=find(peaks<thres);
peaks(exclude)=[];
patchLen=1+2*patchHalfLen;

if isnan(GTbox)
  %no true corresponding point
  TP=0; FP=length(peaks); FN=0;
else
  TP=0; FP=0; FN=1; 
  for k=1:length(peaks)
    [ptx,pty]=ind2sub(size(y),inds(k));
    Pbox=[pty-patchHalfLen,ptx-patchHalfLen,patchLen,patchLen];
    %compare predicted location to expected match location
    BBoverlap=rectOverlap(rectCorners(GTbox),rectCorners(Pbox)); %call BBS code to calc bounding box overlap
    %disp(['GTbox=',num2str(GTbox,'%.1f,'),'  Pbox=',num2str(Pbox,'%.1f,'), '  Overlap=',num2str(BBoverlap)])
    if BBoverlap>=reqdOverlap && TP==0
      %Bounding box overlap with true corresponding location is within threshold
      TP=1; FN=0;
    else
      %Bounding box overlap with true corresponding location is too small
      FP=FP+1;
    end
  end
end
stats=[TP,FP,FN];