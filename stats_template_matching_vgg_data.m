function [stats,f1score]=stats_template_matching_vgg_data(method,imagePairs,sequenceNames,px)
%Tests template matching methods using the dataset used to evaluate image
%descriptor matching: K. Mikolajczyk and C. Schmid, A performance
%evaluation of local descriptors, IEEE Transactions on Pattern Analysis and
%Machine Intelligence, 27(10):1615-1630, 2005.
%
%Templates are extracted from the first image in all sequences. All these
%templates are matched to all the other images in all sequences. The
%precision-recall curve is used to to evaluate performance.

if nargin<1 || isempty(method)
  method='DIM'; %'ZNCC'; %'DDIS'; %'CoTM'; %'BBS'; %
end
if nargin<2 || isempty(imagePairs)
  imagePairs=[1:5];
end
if nargin<3 || isempty(sequenceNames)
  sequenceNames={'Bikes';'Trees';'Leuven';'Wall';'UBC';'Graffiti';'Bark'};
end
if nargin<4 || isempty(px)
  px=1;
end
patchHalfLen=16*px
numTargetsPerImage=10
scale=0.5
reqdOverlap=0.5; %bounding box overlap that consitutes a succesful match

[~,VGGPath]=set_paths(method);
patchLen=1+2*patchHalfLen;
thresholds=[0.001,0.002,0.005,0.01,0.025:0.025:1]; %proportion of maximum similarity 

%EXTRACT ALL TEMPLATES FROM 1st IMAGES, AND ALL TARGET LOCATIONS IN OTHER IMAGES
totalTemplates=0;
for s=1:length(sequenceNames)
  sequenceName=sequenceNames{s};
  disp(['Extracting templates from ',sequenceName]);
    
  %load images in the pair
  if exist([VGGPath,sequenceName,'/img1.ppm'],'file');
    extension='.ppm';
  else
    extension='.pgm';
  end
  I=im2double(imread([VGGPath,sequenceName,'/img1',extension]));
  [a1,b1,c]=size(I);

  %choose locations that are to be matched to other images
  keypoints=extract_keypoints(I,10*numTargetsPerImage,'corner');
  TboxTmp=[keypoints(1,2)-patchHalfLen,keypoints(1,1)-patchHalfLen,patchLen,patchLen];
  keypoints=exclude_keypoints_close_to_border(keypoints,I,TboxTmp);
  for i=imagePairs
    Iquery=im2double(imread([VGGPath,sequenceName,'/img',int2str(1+i),extension])); 
    H(:,:,i)=load([VGGPath,sequenceName,'/H1to',int2str(1+i),'p']); %mapping between points in images
    keypoints=exclude_keypoints_not_visible_after_transform(keypoints,H(:,:,i),Iquery,patchHalfLen);
  end
  keypoints=exclude_keypoints_close_to_others([],keypoints,min(48,TboxTmp),numTargetsPerImage);

  numTemplates(s)=min(size(keypoints,1),numTargetsPerImage);
  keypoints=keypoints(1:numTemplates(s),:);
  
  %convert image to format used by tempalate matching method
  clear X
  switch method
    case 'DIM'
      [I,X]=preprocess(I,patchHalfLen/2,0,[patchLen,patchLen]);
    case 'ZNCC'
      X=rgb2hsv(I);
    case 'BBS'
      X=I;
    case 'DDIS'
      X=I;
    case 'CoTM'
      X=I;
  end
  if scale~=1
    %rescale images
    X=imresize(X,scale);
  end

  %extract templates from target locations in first image
  for t=1:numTemplates(s)
    TBox=[keypoints(t,2)-patchHalfLen,keypoints(t,1)-patchHalfLen,patchLen,patchLen];
    if scale~=1
      TBox=round(TBox.*scale);
    end
    templates{totalTemplates+t}=single(imcrop_odd(X,TBox));
  end
  
  %find corresponding location of each target on each second image
  for i=imagePairs
    for t=1:numTargetsPerImage*length(sequenceNames)
      GTbox{s}{i}(t,:)=[NaN,NaN,NaN,NaN]; %corresponding point only exists for templates in this sequence
    end
    keypoints_trans=project_keypoint(keypoints,H(:,:,i));
    for t=1:numTemplates(s)
      GTbox{s}{i}(totalTemplates+t,:)=[keypoints_trans(t,2)-patchHalfLen,keypoints_trans(t,1)-patchHalfLen,patchLen,patchLen];
    end
    if scale~=1
      GTbox{s}{i}=round(GTbox{s}{i}.*scale);
    end
  end
  totalTemplates=totalTemplates+numTemplates(s);
end
clear I X
totalTemplates
if scale~=1
  patchHalfLen=round(patchHalfLen*scale)
end

%CALCULATE SIMILARITY ARRAY FOR EVERY TEMPLATE APPLIED TO EACH 2nd IMAGE
tic
ymax=0;
for s=1:length(sequenceNames)
  sequenceName=sequenceNames{s};
  for i=imagePairs
    disp(['####### ',sequenceName,' IMAGE ',int2str(i),' #######']);
    %load images in the pair
    if exist([VGGPath,sequenceName,'/','img1.ppm'],'file');
      extension='.ppm';
    else
      extension='.pgm';
    end
    Iquery=im2single(imread([VGGPath,sequenceName,'/','img',int2str(1+i),extension])); 
    [a2,b2,c]=size(Iquery);
    if scale~=1
      %rescale images
      Iquery=imresize(Iquery,scale);
    end
    
    %perform template matching
    switch method
      case 'DIM'
        [y{s}{i}]=template_matching_dim([],[],Iquery,0,[],templates,20);
      case 'ZNCC'
        [y{s}{i}]=template_matching_zncc([],[],Iquery,templates);
      case 'BBS'
        [y{s}{i}]=template_matching_bbs([],[],Iquery,templates);
      case 'CoTM'
        [y{s}{i}]=template_matching_cotm([],[],Iquery,templates);    
      case 'DDIS'
        [y{s}{i}]=template_matching_ddis([],[],Iquery,templates);
    end
    ymax=max(ymax,max(max(max(y{s}{i}))));
  end
end
toc
ymax


%CALC PRECISION RECALL FOR VARYING MATCH ACCEPTANCE THRESHOLDS
fprintf(1,'Analysing matches for threshold ');
stats=zeros(length(thresholds),3);
k=0;
for thres=thresholds
  k=k+1;   
  fprintf(1,'.%1.3f.',thres);
  for s=1:length(sequenceNames)
    for i=imagePairs
      %figured(10*(s-1)+i), clf
      for t=1:totalTemplates
        %disp(['####### ',sequenceName,' IMAGE ',int2str(i),' template ',int2str(t),' #######']);
        %maxsubplot(2,4,t); plot_image(y{s}{i}(:,:,t)); colorbar('SouthOutside'); colormap('gray')
        [statsTmp]=analyse_match_array_threshold(y{s}{i}(:,:,t),patchHalfLen,GTbox{s}{i}(t,:),reqdOverlap,thres*ymax);
        stats(k,:)=stats(k,:)+statsTmp;
      end
    end
  end
  f1score(k)=calc_f1score(stats(k,:));
end
disp(' ');
disp(['f1score: ', num2str(f1score)])
disp(['max f1score: ', num2str(max(f1score))])
figured(201);clf;
plot_precision_recall(stats,method)
legend(method,'Location','NorthWest')

%methods={'ZNCC';'BBS';'DDIS';'DIM'};px=1;
%for i=1:4, [stats{i},f1score{i}]=stats_template_matching_vgg_data(methods{i},[],[],px); end
%for i=1:4, plot_precision_recall(stats{i},methods{i}); hold on; end, 
%if px==1, legend(methods,'Location','NorthWest'); end
%print_fig(['template_matching_vgg_data_halfsize_ALL_',int2str((px*16*scale)*2+1),'px.pdf']); 
