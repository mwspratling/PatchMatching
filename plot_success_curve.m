function [AUC,h]=plot_success_curve(BBoverlap,method)
nPairs=length(BBoverlap);
overlapThres=[0:0.05:1];
for i = 1:length(overlapThres)
  matched(i) = sum(BBoverlap>overlapThres(i))/nPairs;
end
AUC=mean(matched(1:end-1)) %provide quanitative summary of result: this is the method used in the BBS code!
if nargin>1
  lineType=plot_styles(method);
  h=plot(overlapThres,matched,lineType,'linewidth',3,'MarkerSize',10,'MarkerFaceColor',lineType(1));
else
  h=plot(overlapThres,matched,'linewidth',2);
end
hold on
axis('equal','tight');axis([0,1,0,1]);
xlabel('threshold');
ylabel('success rate'); 
set(gca,'FontSize',20);
drawnow