function plot_bounding_box(box,colour)
if nargin<2 || isempty(colour)
  colour='y';
end
hold on
numBoxes=size(box,1);
for i=1:numBoxes
  plot([box(i,1),box(i,1)+box(i,3),box(i,1)+box(i,3),box(i,1),box(i,1)],[box(i,2),box(i,2),box(i,2)+box(i,4),box(i,2)+box(i,4),box(i,2)],colour,'LineWidth',2);
end
colormap('gray')

